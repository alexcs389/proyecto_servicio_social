//= require merge_sort/arrays
//= require merge_sort/settings
//= require merge_sort/animation

$(document).ready(function () {
    //JavaScript de los videos
    const img = document.getElementById('carousel');
    const rightBtn = document.getElementById('right-btn');
    const leftBtn = document.getElementById('left-btn');
    const vurl = document.getElementById('id-video');

    // Lista de los url de los videos
    //let video = ['https://www.youtube.com/embed/vnnL17I7pIY', 
    //'https://www.youtube.com/embed/xli_FI7CuzA', 
    //'https://www.youtube.com/embed/zVjZdrh3tSA'];
    
    // Lista de los url de los videos
    var url1 = $('#id-video1').attr('href');
    var url2 = $('#id-video2').attr('href');   
    var url3 = $('#id-video3').attr('href');

    let videos = [url1, url2, url3]

    img.src = videos[0];
    let position = 0;

    moveRight = () => {
        if (position >= videos.length - 1) {
            position = 0
            img.src = videos[position];
            return;
        }
        img.src = videos[position + 1];
        position++;
    }

    moveLeft = () => {
        if (position < 1) {
            position = videos.length - 1;
            img.src = videos[position];
            return;
        }
        img.src = videos[position - 1];
        position--;
    }

    rightBtn.addEventListener("click", moveRight);
    leftBtn.addEventListener("click", moveLeft);
    //Fin del javascript de los videos

    // DirectMix();
});

function BubbleView(){
    console.log('Prueba de animación');
    //Animación del Bubble Sort
    const boton1 = document.querySelector("#btnPlay");//boton play
    const boton2 = document.querySelector("#btnStop");//boton stop
    const boton3 = document.querySelector("#btnReiniciar");//boton reiniciar
    boton3.disabled = true;
    boton2.disabled = true;
    
    $("#btnPlay").click(function () {
        console.log("Animación")
        boton1.disabled = true;
        boton2.disabled = false;
        boton3.disabled = false;   
        $("#bubbleAni").empty();      
        function principalBubble() {
            const container = document.querySelector("#bubbleAni");
            function generateBlocks(num) {
                if (num && typeof num !== "number") {
                    alert("First argument must be a typeof Number");
                    return;
                }
                for (let i = 0; i < num; i += 1) {
                    const value = Math.floor(Math.random() * 100);

                    const block = document.createElement("div");
                    block.classList.add("block");
                    block.style.height = `${value * 3}px`;
                    block.style.transform = `translateX(${i * 30}px)`;

                    const blockLabel = document.createElement("label");
                    blockLabel.classList.add("block__id");
                    blockLabel.innerHTML = value;

                    block.appendChild(blockLabel);
                    container.appendChild(block);
                }
            }
            function swap(el1, el2) {
                return new Promise(resolve => {
                    const style1 = window.getComputedStyle(el1);
                    const style2 = window.getComputedStyle(el2);

                    const transform1 = style1.getPropertyValue("transform");
                    const transform2 = style2.getPropertyValue("transform");

                    el1.style.transform = transform2;
                    el2.style.transform = transform1;

                    // Wait for the transition to end!
                    window.requestAnimationFrame(function () {
                        setTimeout(() => {
                            container.insertBefore(el2, el1);
                            resolve();
                        }, 250);
                    });
                });
            }
            async function bubbleSort(delay = 150) {
                if (delay && typeof delay !== "number") {
                    alert("EL primer argumento debe de ser un número");
                    return;
                }
                let blocks = document.querySelectorAll(".block");
                for (let i = 0; i < blocks.length - 1; i += 1) {
                    for (let j = 0; j < blocks.length - i - 1; j += 1) {

                        blocks[j].style.backgroundColor = "#FF4949";
                        blocks[j + 1].style.backgroundColor = "#FF4949";

                        await new Promise(resolve =>
                            setTimeout(() => {
                                resolve();
                            }, delay)
                        );

                        const value1 = Number(blocks[j].childNodes[0].innerHTML);
                        const value2 = Number(blocks[j + 1].childNodes[0].innerHTML);

                        if (value1 > value2) {
                            await swap(blocks[j], blocks[j + 1]);
                            blocks = document.querySelectorAll(".block");
                        }
                        blocks[j].style.backgroundColor = "#58B7FF";
                        blocks[j + 1].style.backgroundColor = "#58B7FF";
                    }
                    blocks[blocks.length - i - 1].style.backgroundColor = "#13CE66";
                }
                boton1.disabled = false;
                boton2.disabled = true;
                boton3.disabled = true;
                const blockLabel = document.createElement("label");
                
                //alert("La lista se ha terminado de ordenar");                
            }
            generateBlocks(num = 10);
            bubbleSort();
        };
        principalBubble();
        $("#btnStop").click(function () {
            console.log("Detenido");
            boton1.disabled = false;
            boton2.disabled = true;
            boton3.disabled = true;
            $("#bubbleAni").empty();       
        });
        $("#btnReiniciar").click(function () {
            console.log("Reiniciado");
            boton1.disabled = true;
            $("#bubbleAni").empty();
            principalBubble();                               
        });
    });
    //Fin de la Animación del Bubble Sort

}

function ShellView(){
    //Inicio de la animación del Shell Sort
    console.log("Inicio shellsort");
    const btnIns1 = document.querySelector("#btnPlay");//boton play
    const btnIns2 = document.querySelector("#btnStop");//boton stop
    const btnIns3 = document.querySelector("#btnReiniciar");//boton reiniciar
    //const btnMore = document.querySelector('.form-more');
    btnIns3.disabled = true;
    btnIns2.disabled = true;
    //btnMore.disabled = true;

    $("#btnPlay").click(function () {
        console.log("Animación")
        btnIns1.disabled = true;
        btnIns2.disabled = false;
        btnIns3.disabled = false;
        $(".data-container-shell").empty();
        //document.querySelector(".form-sort");
        document.getElementById("myform").reset();
        //obtiene los valores de los inputs
        var shell = new getData();    
        //aplica el método shellsort a los inputs ingresados
        shell.shellSort();
    });    

    /*function moreField () {
        'use strict';   
        var btnMore = document.querySelector('.form-more'),
            form = document.querySelector('.form-sort'),
            newField,
            count = 9;
    
        btnMore.addEventListener('click', function () {
            ++count;
    
            newField = document.createElement('input');
            newField.setAttribute('type', 'text');
            newField.setAttribute('id', 'f-' + count);
            form.insertBefore(newField, btnMore);
        });
    }
    moreField();      */
    
    function getData () {
        'use strict';    
        var input = document.getElementsByTagName('input'),
            dataArray = [],
            inputValue = [],
            valueOfNumber,
            i,
            len;  
        for (i = 0, len = input.length; len > i; i++) {
            inputValue.push(input[i].value);
            valueOfNumber = inputValue.map(Number);
    
            dataArray.push({
                valueInput: valueOfNumber[0],
                id: input[i].getAttribute('id')
            });    
            inputValue = [];
        }    
        this.dataArray = dataArray;
    }

    getData.prototype.shellSort = function () {
        //'use strict';
    
        var gap = 1,
            j = 0,          
            $this = this,
            dataArray = $this.dataArray,
            afterSortArray = [];
    
        while (gap < dataArray.length / 3) {
            gap = 3 * gap + 1;
            
        }
        while (gap > 0) {
            for (var i = gap; i < dataArray.length; i += gap) {
                for (var n = i; n > 0 && dataArray[n].valueInput < dataArray[n-gap].valueInput; n -= gap) {
                    var number = dataArray[n].valueInput;
    
                    afterSortArray.push({
                        firstId: document.getElementById(dataArray[n].id),
                        secondId: document.getElementById(dataArray[n-gap].id)
                    });
    
                    dataArray[n].valueInput = dataArray[n-gap].valueInput;
                    dataArray[n-gap].valueInput = number;
                }
            }
            gap = --gap / 3;
            
        }           
    
        function showSortSteps () {
            var len = afterSortArray.length,
                firstId,
                secondId,
                firstIdValue,
                secondIdValue;
    
            setTimeout(function () {
    
                try {
                    firstId = afterSortArray[j].firstId;                    
                    
                } catch (e) {
                    document.body.className = 'shell-body';
                }
    
                secondId = afterSortArray[j].secondId;
    
                firstIdValue = firstId.value;
                secondIdValue = secondId.value;
    
                document.body.className = '';
                firstId.setAttribute('class', 'animated zoomIn');
                secondId.setAttribute('class', 'animated zoomIn2');
    
                setTimeout(function () {
                    j++;                     
                    firstId.value = secondIdValue;
                    secondId.value = firstIdValue;
                }, 2000);
    
                if (j < len) {
                    setTimeout(function () {
                        firstId.removeAttribute('class');
                        secondId.removeAttribute('class');
                    }, 2000);                      
                    showSortSteps();
                }               
                
                $("#btnStop").click(function () {
                    console.log("Detenido");
                    btnIns1.disabled = false;
                    btnIns2.disabled = true;
                    btnIns3.disabled = true;
                    //$(".form-sort").empty();
                    //new ShellView();
                    clearIntervalAndTimeout();
                    //$("#f-1").removeAttribute('background-color');
                    firstId.removeAttribute('class');
                    secondId.removeAttribute('class');
                    document.getElementById("myform").reset();
                    
                });
                $("#btnReiniciar").click(function () {
                    console.log("Reiniciado");
                    btnIns1.disabled = true;
                    $(".data-container-shell").empty();
                    clearIntervalAndTimeout();
                    firstId.removeAttribute('class');
                    secondId.removeAttribute('class');
                    document.getElementById("myform").reset();
                    //obtiene los valores de los inputs
                    var shell = new getData();    
                    //aplica el método shellsort a los inputs ingresados
                    shell.shellSort();
                    
                });
                
            }, 2000);
            if(j+1 == len){
                console.log("Termino");
                function habilitarBtn(){
                    setTimeout(function () {
                        btnIns1.disabled = false;
                        btnIns2.disabled = true;
                        btnIns3.disabled = true;
                    }, 2000);
                }
                habilitarBtn();               
            }
        }            
        showSortSteps();           
        
    };    
    function clearIntervalAndTimeout() {
        for (var i = 1; i < 999; i++) {
            window.clearInterval(i);
            window.clearTimeout(i);
        }
    }
    

//Fin de la animación del Shell Sort

}

function InsertionView() {
    //Inicio de la animación del Insertion Sort
    
    const btnIns1 = document.querySelector("#btnPlayIns");//boton play
    const btnIns2 = document.querySelector("#btnStopIns");//boton stop
    const btnIns3 = document.querySelector("#btnReiniciarIns");//boton reiniciar
    btnIns3.disabled = true;
    btnIns2.disabled = true;
    $("#btnPlayIns").click(function () {
        console.log("Animación")
        btnIns1.disabled = true;
        btnIns2.disabled = false;
        btnIns3.disabled = false;   
        $(".data-container").empty();    
        function principalInsertion() {
            const container = document.querySelector("#outDiv");
            var TargetTick;
            var Tick;
            var Timer1;
            var A; // Arreglo a ser ordenado
            var arr0 = "<span id='arr0' >&darr;</span>";
            var arr1 = "<span id='arr1' >&darr;</span>";

            function ExecuteSort() {
                A = [79, 56, 19, 44, 86, 22, 41, 39, 11, 5];
                var Speed = 1000;
                var Alg = 1;
                TargetTick = 1;
                // Start animation
                if (Timer1) clearInterval(Timer1);
                if (Alg == 1)
                    Timer1 = setInterval(AnimateInsertionSort, Speed);                                  
            }          

            function InsertionSort(A) {
                for (var i = 1; i < A.length; i++) {
                    var item = A[i];
                    var j = i - 1;                   
                    while ((j >= 0)) {
                        if (UpdateTick()) { PrintArray(A, j + 1, i, i); return; }
                        if (item >= A[j]) break;

                        A[j + 1] = A[j];
                        A[j] = item; 
                        j--;
                    }
                    A[j + 1] = item;
                }
                PrintArray(A, i + 1, -1, -1);
                EndAnimate();
                btnIns1.disabled = false;
                btnIns2.disabled = true;
                btnIns3.disabled = true;
            }

            function PrintArray(A, start, target, minpos) {                
                var x = "";
                for (var i = 0; i < A.length; i++) {
                    var st = "";
                    if (i < start) st = " class ='finished'";
                    var ext = "";
                    if (i == start) { ext = arr0; st = "style='background-color:#58B7FF'"; }
                    if (i == target) { st = "style='background-color:white'"; }
                    if (i == minpos) { ext += arr1; st = "style='background-color:white;border-color: red'"; }
                    x += "<li " + st + ">" + A[i] + ext + "</li>";
                }
                outDiv.innerHTML = "<ul>" + x + "</ul>";
            }
            function AnimateInsertionSort() { 
                B = A.slice();    
                Tick = 0;
                InsertionSort(B);
                TargetTick++;
            }
            function EndAnimate() { clearInterval(Timer1); }
            function UpdateTick() {
                Tick++;
                return (Tick == TargetTick);
            }
           ExecuteSort();
            $("#btnStopIns").click(function () {
                console.log("Detenido");
                btnIns1.disabled = false;
                btnIns2.disabled = true;
                btnIns3.disabled = true;
                $("#outDiv").empty();
                clearInterval(Timer1);
            });
            $("#btnReiniciarIns").click(function () {
                console.log("Reiniciado");
                btnIns1.disabled = true;
                $("#outDiv").empty();
                clearInterval(Timer1);
                principalInsertion();
            });
        }        
        principalInsertion();           
        
    });
    
//Fin de la animación del Insertion Sort
}

function MergeView() {
    //Inicio de la animación del Merge Sort
    
    const btnIns1 = document.querySelector("#btnPlay");//boton play
    const btnIns2 = document.querySelector("#btnStop");//boton stop
    const btnIns3 = document.querySelector("#btnReiniciar");//boton reiniciar

    btnIns3.disabled = true;
    btnIns2.disabled = true;

    $("#btnPlay").click(function () {
        console.log("Animación")
        btnIns1.disabled = true;
        btnIns2.disabled = false;
        btnIns3.disabled = false;
        $(".data-container").empty();
        var array = getArrayValue();
        sort(array[0]);
    });

    $("#btnStop").click(function () {
        console.log("Detenido");
        btnIns1.disabled = false;
        btnIns2.disabled = true;
        btnIns3.disabled = true;
        $(".data-container").empty();
        clearIntervalAndTimeout();
    });

    $("#btnReiniciar").click(function () {
        console.log("Reiniciado");
        btnIns1.disabled = true;
        $(".data-container").empty();
        clearIntervalAndTimeout();
        var array = getArrayValue();
        sort(array[0]);
    });

    function clearIntervalAndTimeout() {
        for (var i = 1; i < 999; i++) {
            window.clearInterval(i);
            window.clearTimeout(i);
        }
    }

//Fin de la animación del Merge Sort
}

function QuickView() {
    //Inicio de la animación del Quick Sort
    
    const btnIns1 = document.querySelector("#btnPlayQuick");//boton play
    const btnIns2 = document.querySelector("#btnStopQuick");//boton stop
    const btnIns3 = document.querySelector("#btnReiniciarQuick");//boton reiniciar
    btnIns3.disabled = true;
    btnIns2.disabled = true;
    var arr0 = "<span id='arr0' >&darr;</span>";
    var arr1 = "<span id='arr1' >&darr;</span>";   
    const Sort = {
        init: function (options, elem) {
            var self = this;
            self.elem = elem;
            self.$elem = $(elem);
            self.options = $.extend({}, $.fn.animatedSort.options, options);
            self.highlightColor = self.options.highlightColor;
            self.sortedColor = self.options.sortedColor;
            self.animateColor = jQuery.Color ? true : false;
            self.stepTime = self.options.stepTime;
            self.slideTime = self.stepTime * (2.0 / 3);
            self.swapTime = self.stepTime * (15.0 / 24);
            self.colorTime = self.stepTime * (0.5);
            self.sortAlgorithm = self.options.sortAlgorithm;
            self.listType = self.options.listType;
            self.animationTrigger = self.options.animationTrigger;
            self.resetTrigger = self.options.resetTrigger;
            self.callback = self.options.callback;
            self.animSteps = [];
        },
        initList: function () {
            var list = [];
            var self = this;
            self.numbers = self.$elem.find("li");
            self.initialColor = self.numbers.eq(0).css("color");
            self.initialFontSize = self.numbers.eq(0).css("font-size");
            self.slideDistance = Number(self.initialFontSize.substring(0, self.initialFontSize.length - 2)) * 2;
            self.$elem.find("li").each(function () {
                list.push(Number($(this).text()));
                $(this).css({ "position": "relative", "top": 0, "left": 0 });
                if (self.highlightColor !== null && self.sortedColor !== null) {
                    $(this).attr('sorted', 'false');
                }
            });
            self.list = list.slice(0);
            return list;
        },
        randList: function (bottom, top, length) {
            var cantidadNumeros = 10;
            var list = [];
            for (var n = 0; n < length; n++) {
                list.push(Math.floor(Math.random() * (top - bottom) + bottom));
            }
            return list;
        },
        genList: function (list) {
            var self = this;
            var len = list.length;
            self.$elem.append("<ul></ul>");
            for (var n = 0; n < len; n++) {
                self.$elem.children("ul").append("<li>" + list[n] + "</li>");
            }
        },
        setSorted: function (i) {
            var self = this;
            self.highlight([i], self.sortedColor);
        },
        highlight: function (array, color) {
            var self = this;
            if (color !== null) {
                self.animSteps.push(function () {
                    var $liSel = self.numbers.eq(array[0]).add(self.numbers.eq(array[1]));
                    if (self.numbers.eq(0).attr('sorted')) {
                        $liSel = $liSel.filter("[sorted='false']");
                    }
                    if (self.animateColor) {
                        $liSel.animate({ color: color }, self.colorTime, function () {
                            if (color === self.sortedColor) {
                                $liSel.attr('sorted', 'true');
                            }
                        });
                    } else {
                        $liSel.css("color", color);
                        if (color === self.sortedColor) {
                            $liSel.attr('sorted', 'true');
                        }
                    }
                });
            }
        },
        addHighlightColor: function (array) {
            var self = this;
            self.highlight(array, self.highlightColor);
        },

        removeHighlightColor: function (array) {
            var self = this;
            self.highlight(array, self.initialColor);
        },
        slide: function (array, distance) {
            var self = this;
            var $liSel = self.numbers.eq(array[0]);
            for (var n = 1; n < array.length; n++) {
                $liSel = $liSel.add(self.numbers.eq(array[n]));
            }
            self.animSteps.push(function () {
                $liSel.animate({ left: distance }, self.slideTime);
            });
        },

        slideOut: function (array) {
            var self = this;
            self.slide(array, self.slideDistance);
        },

        slideIn: function (array) {
            var self = this;
            self.slide(array, 0);
        },

        swap: function (list, i1, i2) {
            var self = this;
            var temp = list[i1];
            list[i1] = list[i2];
            list[i2] = temp;
            var $li1 = self.numbers.eq(i1);
            var $li2 = self.numbers.eq(i2);
           
            self.animSteps.push(function () {
                var li1_val = $li1.text();
                var li2_val = $li2.text();
                var li1_pos = $li1.position().top;
                var li2_pos = $li2.position().top;
                var li1_left = $li1.css("left");
                var li2_left = $li2.css("left");
                var li1_color = $li1.css("color");
                var li2_color = $li2.css("color");
                var li1_sorted = $li1.attr("sorted");
                var li2_sorted = $li2.attr("sorted");

                $li1.animate({ top: li2_pos - li1_pos }, self.swapTime, function () {
                    $li1.css({ top: 0, left: li2_left, backgroundColor: "#13CE66" });
                    $li1.css({ top: 0, left: li2_left, color: li2_color });
                    $li1.text(li2_val);
                    $li1.attr("sorted", li2_sorted);
                });
                $li2.animate({ top: li1_pos - li2_pos }, self.swapTime, function () {
                    $li2.css({ top: 0, left: li1_left, color: "li1_color" });
                    //$li2.css({ top: 0, left: li1_left, backgroundColor: "#13CE66" });
                    $li2.text(li1_val);
                    $li2.attr("sorted", li1_sorted);
                });
            });
        },

        slideSwap: function (list, i1, i2) {
            var self = this;
            self.slideOut([i1, i2]);
            self.swap(list, i1, i2);
            self.slideIn([i1, i2]);
        },

        animation: function () {
            var self = this;
            if (self.animSteps.length) {
                setTimeout(function () {
                    self.animSteps.splice(0, 1)[0]();
                    self.animation();
                }, self.stepTime);
            }
        },

        quick: function (list) {
            var self = this;
            var len = list.length;
            function partition(array, begin, end, pivot) {
                var pivotValue = array[pivot];
                self.addHighlightColor([pivot]);
                
                if (pivot !== end) {
                    self.slideSwap(array, pivot, end);                                      
                }
                var store = begin;
                for (var n = begin; n < end; n++) {
                    self.addHighlightColor([n]);
                    if (array[n] < pivotValue) {
                        if (store !== n) {
                            self.slideSwap(array, store, n);
                            self.removeHighlightColor([store]);
                        }
                        else {
                            self.removeHighlightColor([n]);
                        }
                        store++;
                    }
                    else {
                        self.removeHighlightColor([n]);
                    }
                }
                if (end !== store) {
                    self.slideSwap(array, end, store);
                }
                self.removeHighlightColor([store]);
                self.setSorted(store);
                return store;
            }

            function quickSort(array, begin, end) {
                if (end > begin) {
                    var pivot = begin + Math.floor(Math.random() * (end - begin));
                    
                    pivot = partition(array, begin, end, pivot);
                    quickSort(array, begin, pivot);
                    quickSort(array, pivot + 1, end);
                }
                else {
                    self.setSorted(begin);
                }
            }
            quickSort(list, 0, len - 1);
        }
    };

    $.fn.animatedSort = function (options) {
        return this.each(function () {
            var sort = Object.create(Sort);
            sort.init(options, this);
            if ($.isArray(sort.listType)) {
                sort.genList(sort.listType)
            }
            else if (typeof (sort.listType) === "object") {
                sort.genList(sort.randList(sort.listType.bottom, sort.listType.top, sort.listType.length));
            }
            sort[sort.sortAlgorithm](sort.initList());
            if (typeof (sort.callback) === "function") {
                var self = this;
                sort.animSteps.push(function () { sort.callback.call(self) });
            }
           
            $("#btnPlayQuick").click(function () {
                console.log("Play");
                btnIns1.disabled = true;
                btnIns2.disabled = false;
                btnIns3.disabled = false;
                sort.animation();
                
            });
            $("#btnReiniciarQuick").click(function () {
                console.log("Reiniciado");
                btnIns1.disabled = true;   
                //sort.animSteps.push(function () { sort.callback.call(self) });                   
                sort.$elem.find("ul").eq(0).remove();
                sort.options.listType = sort.list;
                sort.$elem.animatedSort(sort.options);
                sort.animation();
                
            });

            $("#btnStopQuick").click(function () {
                console.log("Detenido");
                btnIns1.disabled = false;
                btnIns2.disabled = true;
                btnIns3.disabled = true;
                sort.$elem.find("ul").eq(0).remove();
                sort.options.listType = sort.list;
                sort.$elem.animatedSort(sort.options);
                
            });
        });
    };

    $.fn.animatedSort.options = {
        sortAlgorithm: "quick",    // string for type of sort
        highlightColor: "red",      // highlight color (null sets no color)
        sortedColor: "blue",        // sorted color (null sets to no color)
        stepTime: 1000,             // ms between animation steps
        listType: "existing",       // "existing", object for random , array
        animationTrigger: null,     // animation trigger "none" loads on document, object for event and selector
        resetTrigger: null,         // trigger to reset and reinitialize
        callback: null              // callback after animation completes
    };
    
    $("#container").animatedSort({
        stepTime: 600,
        listType: { bottom: 0, top: 100, length: 10 },
        sortAlgorithm: "quick",
        highlightColor: "red",
        sortedColor: "black",
        animationTrigger: { event: "click", selector: "#btnPlayQuick" },
        resetTrigger: { event: "click", selector: "#btnReiniciarQuick" }
    });
    
//Fin de la animación del Quick Sort
}

class DirectMixAnimate {
    F1 = [];
    F2 = [];
    F = [9, 75, 14, 68, 29, 17, 31, 25, 4, 5, 13, 18, 72 ,46, 61];
    TIME = 2000;
    stop = false;

    construtor() {}

    stopAnimation() {
        this.stop = true;
    }

    animate() {
        this.F1 = [];
        this.F2 = [];
        this.F = [9, 75, 14, 68, 29, 17, 31, 25, 4, 5, 13, 18, 72 ,46, 61];
        this.stop = false;
        return new Promise(async (resolve, reject) => {
            await this.addRow(this.F, 'F');
            if (this.stop) {
                throw 'User stop';
            }
            await this.divideArray(this.F, 1);
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F1, 'F1');
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F2, 'F2');
            if (this.stop) {
                throw 'User stop';
            }
            await this.joinArrays(this.F1, this.F2, 2);
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F, 'F');
            if (this.stop) {
                throw 'User stop';
            }
            await this.divideArray(this.F, 2);
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F1, 'F1');
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F2, 'F2');
            if (this.stop) {
                throw 'User stop';
            }
            await this.joinArrays(this.F1, this.F2, 4);
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F, 'F');
            if (this.stop) {
                throw 'User stop';
            }
            await this.divideArray(this.F, 4);
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F1, 'F1');
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F2, 'F2');
            if (this.stop) {
                throw 'User stop';
            }
            await this.joinArrays(this.F1, this.F2, 8);
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F, 'F');
            if (this.stop) {
                throw 'User stop';
            }
            await this.divideArray(this.F, 8);
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F1, 'F1');
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F2, 'F2');
            if (this.stop) {
                throw 'User stop';
            }
            await this.joinArrays(this.F1, this.F2, 16);
            if (this.stop) {
                throw 'User stop';
            }
            await this.addRow(this.F, 'F');
            if (this.stop) {
                throw 'User stop';
            }

            resolve();
        });
    }

    addRow(arr, name) {
        return new Promise(resolve => {
            setTimeout(() => {
                const arrow_class = name.toLowerCase();
                const container = $('<div></div>').addClass(`arr-row row-${arrow_class}`);
                container.append(`<label>${name}:</label>`);
                arr.forEach(function (item) {
                    const value = $('<div></div>').text(item).addClass(`arr-row-item arr-row-${arrow_class}`);
                    container.append(value);
                }, this);
                if (this.stop) {
                    throw 'User stop';
                }
                $('section.container-data').append(container[0]);
                resolve();
            }, this.TIME);
        });
    }

    async divideArray(arr, lng) {
        this.F1 = [];
        this.F2 = [];
        let band = true;
        while (arr.length > 0) {
            if (this.stop) {
                throw 'User stop';
            }
            const value = arr.splice(0, lng);
            if (band) {
                this.F1 = [...this.F1, ...value];
                await this.animateDivideArray(this.F1, 'F1');
                band = false;
            } else {
                this.F2 = [...this.F2, ...value];
                await this.animateDivideArray(this.F2, 'F2');
                band = true;
            }
        }
    }

    async animateDivideArray(arr, typeItem) {
        const row = $('section.container-data').children('.row-f').last();
        const rowItems = row.children('.arr-row-item');
        await this.timer(500);
        for (let j = 0; j < arr.length; j++) {
            for (let i = 0; i < rowItems.length; i++) {
                const child = $(rowItems[i]);
                const textItem = parseInt(child.text());
                if (this.stop) {
                    throw 'User stop';
                }
                if (arr[j] === textItem) {
                    if (typeItem === 'F1') {
                        child.addClass('item-f1');
                    } else {
                        child.addClass('item-f2');
                    }
                }
            }
        }
    }

    async joinArrays(arr1, arr2, lng) {
        return new Promise(resolve => {
           setTimeout(async () => {
               this.F = [];
               let arrTemp = [];
               let value1;
               let value2;
               const size = lng / 2;

               while (arr1.length > 0 || arr2.length > 0) {
                   value1 = null;
                   value2 = null;
                   arrTemp = [];
                   if (this.stop) {
                       throw 'User stop';
                   }
                   if (arr1.length > 0) {
                       value1 = arr1.splice(0, size);
                   }
                   if (arr2.length > 0) {
                       value2 = arr2.splice(0, size);
                   }
                   if (value1 && value1.length > 0) {
                       arrTemp = [...arrTemp, ...value1];
                   }
                   if (value2 && value2.length > 0) {
                       arrTemp = [...arrTemp, ...value2];
                   }
                   if (arrTemp.length > 1) {
                       let done = false;
                       const arrOrdered = [...arrTemp];
                       while(!done){
                           done = true;
                           for(let i = 1; i < arrOrdered.length; i++){
                               const num1 = arrOrdered[i - 1];
                               const num2 = arrOrdered[i];

                               await this.animateJoinArrays(num1, num2);

                               if(num1 > num2){
                                   done = false;
                                   const tmp = arrOrdered[i - 1];
                                   arrOrdered[i - 1] = num2;
                                   arrOrdered[i] = tmp;
                               }
                           }
                       }
                       this.F = [...this.F, ...arrOrdered];
                   } else {
                       await this.animateJoinArrays(arrTemp[0], null);
                       this.F = [...this.F, ...arrTemp];
                   }
               }
               resolve();
           }, this.TIME);
        });
    }

    async animateJoinArrays(num1, num2) {
        let element1 = null;
        let element2 = null;
        if (this.stop) {
            throw 'User stop';
        }
        if (num1) {
            element1 = await this.findItem(num1);
            element1.removeClass('arr-row-f1 arr-row-f2 min-item max-item').addClass('compare-items');
        }
        if (num2) {
            element2 = await this.findItem(num2);
            element2.removeClass('arr-row-f1 arr-row-f2 min-item max-item').addClass('compare-items');
        }

        await this.timer(2000);

        if (this.stop) {
            throw 'User stop';
        }
        if (num1 && num2) {
            if (num1 > num2) {
                element2.removeClass('compare-items').addClass('min-item');
                element1.removeClass('compare-items').addClass('max-item');
            } else {
                element1.removeClass('compare-items').addClass('min-item');
                element2.removeClass('compare-items').addClass('max-item');
            }
        } else if (num1) {
            element1.removeClass('compare-items').addClass('min-item');
        } else if (num2) {
            element2.removeClass('compare-items').addClass('min-item');
        }
    }

    findItem(num) {
        const container = $('section.container-data');
        const rowF1 = container.children('.row-f1').last();
        const rowItemsF1 = rowF1.children('.arr-row-item');

        const rowF2 = container.children('.row-f2').last();
        const rowItemsF2 = rowF2.children('.arr-row-item');

        for (let i = 0; i < rowItemsF1.length; i++) {
            const child = $(rowItemsF1[i]);
            const textItem = parseInt(child.text());
            if (textItem === num) {
                return child;
            }
        }
        for (let j = 0; j < rowItemsF2.length; j++) {
            const child = $(rowItemsF2[j]);
            const textItem = parseInt(child.text());
            if (textItem === num) {
                return child;
            }
        }
    }

    timer(ms) {
        return new Promise(res => setTimeout(res, ms));
    }
}

function DirectMix() {
    const boton1 = document.querySelector("#btnPlayDirectMix");
    const boton2 = document.querySelector("#btnStopDirectMix");
    const boton3 = document.querySelector("#btnReiniciarDirectMix");
    boton3.disabled = true;
    boton2.disabled = true;
    const animatePromise = new DirectMixAnimate();

    $("#btnPlayDirectMix").click(function () {
        console.log("Animación")
        boton1.disabled = true;
        boton2.disabled = false;
        boton3.disabled = false;

        $('section.container-data').empty();
        animatePromise.animate();
    });
    $("#btnStopDirectMix").click(function () {
        console.log("Detenido");
        boton1.disabled = false;
        boton2.disabled = true;
        boton3.disabled = true;

        animatePromise.stopAnimation();
        $('section.container-data').empty();
    });
    $("#btnReiniciarDirectMix").click(function () {
        console.log("Reiniciado");
        boton1.disabled = true;

        animatePromise.stopAnimation();
        $('section.container-data').empty();
        setTimeout(() => {
            animatePromise.animate();
        }, 3000);
    });
}

class BalancedMixAnimate {
    F1 = [];
    F1_AUX = [];
    F2 = [];
    F2_AUX = [];
    F = [9, 75, 14, 68, 29, 17, 31, 25, 4, 5, 13, 18, 72 ,46, 61];
    TIME = 2000;
    stop = false;

    constructor() {}

    stopAnimation() {
        this.stop = true;
    }

    async animate() {
        this.F1 = [];
        this.F1_AUX = [];
        this.F2 = [];
        this.F2_AUX = [];
        this.F = [9, 75, 14, 68, 29, 17, 31, 25, 4, 5, 13, 18, 72 ,46, 61];
        this.stop = false;

        await this.addRow(this.F, 'F');
        if (this.stop) {
            throw 'User stop';
        }
        await this.particionInicial();
        if (this.stop) {
            throw 'User stop';
        }
        await this.addRow(this.F1, 'F1', true);
        if (this.stop) {
            throw 'User stop';
        }
        await this.addRow(this.F2, 'F2', true);
        if (this.stop) {
            throw 'User stop';
        }
        await this.particionInicial(true);
        if (this.stop) {
            throw 'User stop';
        }
        await this.particionFusion();
        if (this.stop) {
            throw 'User stop';
        }
        await this.addRow(this.F, 'F');
        if (this.stop) {
            throw 'User stop';
        }
        await this.particionInicial();
        if (this.stop) {
            throw 'User stop';
        }
        await this.addRow(this.F1, 'F1', true);
        if (this.stop) {
            throw 'User stop';
        }
        await this.addRow(this.F2, 'F2', true);
        if (this.stop) {
            throw 'User stop';
        }
        await this.particionInicial(true);
        if (this.stop) {
            throw 'User stop';
        }
        await this.particionFusion();
        if (this.stop) {
            throw 'User stop';
        }
        await this.addRow(this.F, 'F');
        if (this.stop) {
            throw 'User stop';
        }
        await this.particionInicial();
        if (this.stop) {
            throw 'User stop';
        }
        await this.addRow(this.F1, 'F1', true);
        if (this.stop) {
            throw 'User stop';
        }
        await this.addRow(this.F2, 'F2', true);
        if (this.stop) {
            throw 'User stop';
        }
        await this.particionInicial(true);
        if (this.stop) {
            throw 'User stop';
        }
        await this.particionFusion();
        if (this.stop) {
            throw 'User stop';
        }
        await this.addRow(this.F, 'F');
    }

    particionInicial(animate = false) {
        return new Promise(resolve => {
            setTimeout(async () => {
                this.F1 = [];
                this.F1_AUX = [];
                this.F2 = [];
                this.F2_AUX = [];

                let lastValue = 0;
                let currentArr = 'F1';
                let F1_PAIRS = [];
                let F2_PAIRS = [];

                let num1Element;
                let num2Element;

                if (this.stop) {
                    throw 'User stop';
                }
                for (let i = 0; i < this.F.length; i++) {
                    const value = this.F[i];

                    if (this.stop) {
                        throw 'User stop';
                    }
                    if (animate) {
                        num1Element = this.findElement('F', value);
                        num1Element.addClass('to-comparate');

                        if (lastValue > 0) {
                            num2Element = this.findElement('F', lastValue);
                            num2Element.addClass('to-comparate');
                        }
                        await this.timer(1000);
                    }
                    if (this.stop) {
                        throw 'User stop';
                    }

                    if (value > lastValue) {
                        if (this.stop) {
                            throw 'User stop';
                        }
                        if (currentArr === 'F1') {
                            this.F1.push(value);
                            F1_PAIRS.push(value);

                            if (animate) {
                                num1Element.removeClass('to-comparate').addClass(`sub-list-${this.F1_AUX.length}`);

                                const f1Element = this.findElement('F1', value);
                                f1Element.removeClass('hide').addClass(`sub-list-${this.F1_AUX.length} show`);
                            }
                            if (this.stop) {
                                throw 'User stop';
                            }
                        } else {
                            this.F2.push(value);
                            F2_PAIRS.push(value);

                            if (animate) {
                                num1Element.removeClass('to-comparate').addClass(`sub-list-${this.F2_AUX.length}`);

                                const f2Element = this.findElement('F2', value);
                                f2Element.removeClass('hide').addClass(`sub-list-${this.F2_AUX.length} show`);
                            }
                            if (this.stop) {
                                throw 'User stop';
                            }
                        }
                        if (this.stop) {
                            throw 'User stop';
                        }
                        if (num2Element) {
                            num2Element.removeClass('to-comparate');
                        }
                        if ((i + 1) === this.F.length) {
                            if (F1_PAIRS.length > 0) {
                                this.F1_AUX.push(F1_PAIRS);
                                F1_PAIRS = [];
                            }
                            if (F2_PAIRS.length > 0) {
                                this.F2_AUX.push(F2_PAIRS);
                                F2_PAIRS = [];
                            }
                        }
                        lastValue = value;
                    } else {
                        if (this.stop) {
                            throw 'User stop';
                        }
                        if (F1_PAIRS.length > 0) {
                            this.F1_AUX.push(F1_PAIRS);
                            F1_PAIRS = [];
                        }
                        if (F2_PAIRS.length > 0) {
                            this.F2_AUX.push(F2_PAIRS);
                            F2_PAIRS = [];
                        }

                        currentArr = (currentArr === 'F1') ? 'F2' : 'F1';
                        if (currentArr === 'F1') {
                            this.F1.push(value);
                            F1_PAIRS.push(value);
                            if (animate) {
                                if (this.stop) {
                                    throw 'User stop';
                                }
                                num1Element.removeClass('to-comparate').addClass(`sub-list-${this.F1_AUX.length}`);

                                const f1Element = this.findElement('F1', value);
                                f1Element.removeClass('hide').addClass(`sub-list-${this.F1_AUX.length} show`);
                            }
                            if (this.stop) {
                                throw 'User stop';
                            }
                        } else {
                            this.F2.push(value);
                            F2_PAIRS.push(value);
                            if (animate) {
                                if (this.stop) {
                                    throw 'User stop';
                                }
                                num1Element.removeClass('to-comparate').addClass(`sub-list-${this.F2_AUX.length}`);

                                const f2Element = this.findElement('F2', value);
                                f2Element.removeClass('hide').addClass(`sub-list-${this.F2_AUX.length} show`);
                            }
                            if (this.stop) {
                                throw 'User stop';
                            }
                        }
                        if (num2Element) {
                            num2Element.removeClass('to-comparate');
                        }
                        if (this.stop) {
                            throw 'User stop';
                        }
                        lastValue = value;
                    }
                }
                resolve();
            }, this.TIME);
        });
    }

    particionFusion() {
        return new Promise(resolve => {
            setTimeout(async () => {
                this.F = [];
                let size = (this.F1_AUX.length > this.F2_AUX.length) ? this.F1_AUX.length : this.F2_AUX.length;
                for (let i = 0; i < size; i++) {
                    if (this.stop) {
                        throw 'User stop';
                    }

                    const arr1 = this.F1_AUX[i] || [];
                    const arr2 = this.F2_AUX[i] || [];
                    const mainArr = [...arr1, ...arr2];

                    while (mainArr.length > 0) {
                        if (this.stop) {
                            throw 'User stop';
                        }

                        const min = Math.min(...mainArr);
                        this.F.push(min);

                        const index = mainArr.indexOf(min);
                        if (index > -1) {
                            mainArr.splice(index, 1);
                        }

                        let elementList = this.findElement('F1', min);
                        if (!elementList) {
                            elementList = this.findElement('F2', min);
                        }

                        await this.timer(1000);

                        if (this.stop) {
                            throw 'User stop';
                        }

                        elementList.removeClass(`sub-list-${i}`).addClass('sub-list-min');
                    }
                }
                resolve();
            }, this.TIME);
        });
    }

    findElement(arrType, value) {
        let classToFind = '';
        if (arrType === 'F') {
            classToFind = 'arr-list-f';
        } else if (arrType === 'F1') {
            classToFind = 'arr-list-f1';
        } else {
            classToFind = 'arr-list-f2';
        }

        const container = $('.container-data');
        const arrList = container.children(`.${classToFind}`).last();
        const arrListItems = arrList.children('.arr-list-item');

        for (let i = 0; i < arrListItems.length; i++) {
            const child = $(arrListItems[i]);
            const textItem = parseInt(child.text());
            if (textItem === value) {
                return child;
            }
        }
    }

    addRow(arr, name, hide = false) {
        return new Promise(resolve => {
            setTimeout(() => {
                const arrow_class = name.toLowerCase();
                const container = $('<div></div>').addClass(`arr-list arr-list-${arrow_class}`);
                container.append(`<label>${name}:</label>`);
                if (this.stop) {
                    throw 'User stop';
                }
                arr.forEach(function (item) {
                    if (this.stop) {
                        throw 'User stop';
                    }
                    const value = $('<div></div>').text(item).addClass('arr-list-item');
                    if (hide) {
                        value.addClass('hide');
                    } else {
                        value.addClass('show');
                    }
                    container.append(value);
                }, this);
                $('.container-data').append(container[0]);
                resolve();
            }, this.TIME);
        });
    }

    timer(ms) {
        return new Promise(res => setTimeout(res, ms));
    }
}

function BalancedMix() {
    const boton1 = document.querySelector("#btnPlayBalancedMix");
    const boton2 = document.querySelector("#btnStopBalancedMix");
    const boton3 = document.querySelector("#btnReiniciarBalancedMix");
    boton3.disabled = true;
    boton2.disabled = true;
    const animatePromise = new BalancedMixAnimate();

    $("#btnPlayBalancedMix").click(function () {
        console.log("Animación")
        boton1.disabled = true;
        boton2.disabled = false;
        boton3.disabled = false;

        $('section.container-data').empty();
        animatePromise.animate();
    });
    $("#btnStopBalancedMix").click(function () {
        console.log("Detenido");
        boton1.disabled = false;
        boton2.disabled = true;
        boton3.disabled = true;

        animatePromise.stopAnimation();
        $('section.container-data').empty();
    });
    $("#btnReiniciarBalancedMix").click(function () {
        console.log("Reiniciado");
        boton1.disabled = true;

        animatePromise.stopAnimation();
        $('section.container-data').empty();
        setTimeout(() => {
            animatePromise.animate();
        }, 3000);
    });
}